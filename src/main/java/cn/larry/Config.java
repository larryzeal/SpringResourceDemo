package cn.larry;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import cn.larry.bean.OneBean;

/**
 * @author LarryZeal
 *
 */
@Configuration
@ImportResource("classpath:applicationContext.xml") // 奇怪，通过XML构造，并注值。居然先于@PostContruct method。
public class Config {
	@Autowired
	private ApplicationContext ctx;
	
	@Autowired
	private ResourceLoader loader;

	// 看这个就知道为什么注值会先于@PostConstruct了。
	@Bean
	public OneBean two() {
		OneBean two = new OneBean();
		// loader.getResource(location)
		two.setResource(ctx.getResource("classpath:abc.properties"));
		return two;
	}
	
	@PostConstruct
	public ApplicationContext fileCtx(){
		ApplicationContext ctx=new FileSystemXmlApplicationContext();
		System.out.println(ctx.getResource("/abc.properties"));
		System.out.println(ctx.getResource("abc.properties"));
		return ctx;
	}
}
