package cn.larry.bean;

/**
 * 这个类用来测试propertyeditor。
 * 按照标准JavaBean的说法，会只要是同包下名字相同（但propertyeditor以Editor结尾），就会自动注册propertyeditor。
 * 
 * @author LarryZeal
 *
 */
public class Foo {
	private String	name;
	private int		age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
