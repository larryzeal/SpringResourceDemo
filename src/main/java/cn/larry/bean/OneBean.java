package cn.larry.bean;

import javax.annotation.PostConstruct;

import org.springframework.core.io.Resource;

/**
 * @author LarryZeal
 *
 */
public class OneBean {
	private Resource resource;

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	@PostConstruct
	public void run() {
		System.out.println(resource + " : " + resource.exists());

	}
}
