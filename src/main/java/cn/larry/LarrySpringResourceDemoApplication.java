package cn.larry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackageClasses=Config.class)
public class LarrySpringResourceDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(LarrySpringResourceDemoApplication.class, args);
	}
}
